#include <SpacebrewYun.h>

/*define logic control output pin*/
//For Car 3.0
int in1=7;
int in2=8;
int in3=9;
int in4=11;
/*define channel enable output pins*/
int ENA=6;
int ENB=5;


/*Slow Speed of Car*/
int Sspd_L = 50;
int Sspd_R = 50;

/*Normal Speed of Car (0-255) */
int Nspd_L = 95;
int Nspd_R = 95;

/*Fast Speed of Car*/
int Fspd_L = 145;
int Fspd_R = 145;

/*Fast Fast Speed of Car*/
int FFspd_L = 225;
int FFspd_R = 225;
void setup() {

 /*Open the serial port and set the baud rate to 9600 */
 Serial.begin(9600); 
 
/*Set the defined pins to the output*/
  pinMode(in1,OUTPUT);
  pinMode(in2,OUTPUT);
  pinMode(in3,OUTPUT);
  pinMode(in4,OUTPUT);
  pinMode(ENA,OUTPUT);
  pinMode(ENB,OUTPUT);

  digitalWrite(ENA, LOW);
  digitalWrite(ENB, LOW);

/* wait for serial port to connect. */
  while (!Serial) {
    ; 
  
}
}
/*define forward function*/
void _mForward_N()
{ 
  analogWrite(ENA, Nspd_L);
  analogWrite(ENB, Nspd_R);
  digitalWrite(in1,HIGH);//digital output
  digitalWrite(in2,LOW);
  digitalWrite(in3,LOW);
  digitalWrite(in4,HIGH);
  /*Serial.println("Forward");*/
}

void _mForward_S()
{ 
  analogWrite(ENA, Sspd_L);
  analogWrite(ENB, Sspd_R);
  digitalWrite(in1,HIGH);//digital output
  digitalWrite(in2,LOW);
  digitalWrite(in3,LOW);
  digitalWrite(in4,HIGH);
  /*Serial.println("Forward");*/
}

void _mForward_F()
{ 
  analogWrite(ENA, Fspd_L);
  analogWrite(ENB, Fspd_R);
  digitalWrite(in1,HIGH);//digital output
  digitalWrite(in2,LOW);
  digitalWrite(in3,LOW);
  digitalWrite(in4,HIGH);
  /*Serial.println("Forward");*/
}

/*define back function*/
void _mBack()
{
 analogWrite(ENA, Nspd_R);
  analogWrite(ENB, Fspd_L);
  digitalWrite(in1,LOW);
  digitalWrite(in2,HIGH);
  digitalWrite(in3,HIGH);
  digitalWrite(in4,LOW);
  /*Serial.println("Back");*/
}

/*define left function*/
void _mleft()
{
  analogWrite(ENA, Fspd_R);
  analogWrite(ENB, Fspd_L);
  digitalWrite(in1,LOW);
  digitalWrite(in2,LOW);
  digitalWrite(in3,LOW);
  digitalWrite(in4,HIGH); 
  /*Serial.println("Left"); */
}

/*define right function*/
void _mright()
{
  analogWrite(ENA, Fspd_R);
  analogWrite(ENB, Fspd_L);
  digitalWrite(in1,HIGH);
  digitalWrite(in2,LOW);
  digitalWrite(in3,LOW);
  digitalWrite(in4,LOW);
  /*Serial.println("Right"); */
}

void _mReturn()
{
  analogWrite(ENA, FFspd_R);
  analogWrite(ENB, FFspd_L);
  digitalWrite(in1,LOW);
  digitalWrite(in2,HIGH);
  digitalWrite(in3,LOW);
  digitalWrite(in4,HIGH);  
  }

void _mStop()
{
  digitalWrite(ENA, LOW);
  digitalWrite(ENB, LOW);

  /*Serial.println("Left"); */
}


void loop() {
  char buffer[16];
  
  /*if we get a command */
  if (Serial.available() > 0) {
    int size = Serial.readBytesUntil('\n', buffer, 12);
    if (buffer[0] == 'W') {
      _mForward_N();
    }
    if (buffer[0] == 'A') {
      _mleft();
    }
    
    if (buffer[0] == 'S') {
      _mBack();
    }
    
    if (buffer[0] == 'D') {
      _mright();
    }
    
    if (buffer[0] == 'Q'){
      _mStop();
    }   

    if (buffer[0] == 'T'){
     _mReturn();
    }   
  }
}
