# UTK project - Summer 2019
# Dr. Alan Ayala
# Patrick Lau
# Brendan Flood
# Julian Halloy
# =================================================
from imageai.Prediction.Custom import ModelTraining
from imageai.Prediction import ImagePrediction


import os
import numpy as np
import cv2
import serial
import time

# ==========
# Prediction
# ==========
from imageai.Prediction.Custom import CustomImagePrediction


prediction = CustomImagePrediction()

# 4 pre-trained network is avaliable to use
#prediction.setModelTypeAsSqueezeNet()
#prediction.setModelTypeAsInceptionV3()
#prediction.setModelTypeAsDenseNet()
# ResNet is used in this project
prediction.setModelTypeAsResNet()

# Load prediction model (don't need to retrain)
# Change the path by yourself
execution_path = os.getcwd()
prediction.setModelPath(os.path.join(execution_path, "7-30-test/model.h5") )
prediction.setJsonPath( os.path.join(execution_path, "7-30-test/model_class.json") )


#=============
# define here
#=============
# Camera

# Images are captured in 144p when the car is driving (To reduce the memory usage)
# 1080p is great, HD and same as training data, but Jetson Nano will probably runs out of memory
def gstreamer_pipeline (capture_width=256, capture_height=144, display_width=256, display_height=144, framerate=2, flip_method=2) :
    return ('nvarguscamerasrc ! '
    'video/x-raw(memory:NVMM), '
    'width=(int)%d, height=(int)%d, '
    'format=(string)NV12, framerate=(fraction)%d/1 ! '
    'nvvidconv flip-method=%d ! '
    'video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! '
    'videoconvert ! '
    'video/x-raw, format=(string)BGR ! appsink'  % (capture_width,capture_height,framerate,flip_method,display_width,display_height))

cap = cv2.VideoCapture(gstreamer_pipeline(flip_method=2), cv2.CAP_GSTREAMER)

# Some variables for testing
exPrediction =  ''
cuPrediction = ''
counter = 0
prob = 0
turn = 0

# Arduino
# Locate the port that used to connect with the arduino
# Check the port by command 
# $ ls -l /dev/ttyACM*

# Before running this code, give permission to read / write the data first
# $ sudo chmod a+rw /dev/ttyACM0   (Could be ttyACM1 ttyACM2 ...)
with serial.Serial('/dev/ttyACM0', 9600, timeout=10) as ser:


# ====================
# begin loop for saving images, running prediction, and outputting command to arduino
# ====================
    while True:

        ret, frame = cap.read()
        if(turn == turn):
            if ret==True:
                cv2.imwrite('image.jpg', frame)
                # Specifies display size
                #display = cv2.resize(frame,(426,240))
                #cv2.imshow('display',display)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            else:
                break
        
            # num_objects = number of labels in your model
            # In our case, we have "L" "W" "R" "TL" "TR" "RE", total 6 labels
            prediction.loadModel(num_objects=6, prediction_speed="fastest")

            # The model read image from the path and predict 
            # result_count can be 1 - 6.
            # result_count = 1 will show the most possible prediction
            predictions, probabilities = prediction.predictImage(os.path.join(execution_path, "image.jpg"), result_count=1)
            for eachPrediction, eachProbability in zip(predictions, probabilities):
                print(eachPrediction, eachProbability)

                #Just for testing
                #(*)-- Or you may use it to add condition for sending command to the arduino
                exPrediction = cuPrediction
                #print("Previous-Prediction = ", exPrediction)
                cuPrediction = eachPrediction
                #print("Current-Prediction = ", cuPrediction)

            if exPrediction == cuPrediction:
                counter += 1
                #print("Counter = ", counter)
                exPrediction = cuPrediction
            else:
                counter = 0

                prob = float(eachProbability)        
                prob = round(prob)
            
            # (*)-- Counter counts the number of consecutive predictions
            # Use it as condition if you want the car becomes more stable
            # However, delay may occurs
            #if (counter > 2) 


            # Block the random prediction which has low probability but already the most possible prediction
            if (prob > 30):
                if eachPrediction == 'W':
                    ser.write(bytes('W\n','utf-8'))
                    turn = 0
                    print('forward')
              

                if  eachPrediction == 'L':
                    ser.write(bytes('A\n','utf-8'))
                    turn = 0
                    print('left')


                if  eachPrediction == 'R':
                    ser.write(bytes('D\n','utf-8'))
                    turn = 0
                    print('right')


                if  eachPrediction == 'TL':
                    ser.write(bytes('R\n', 'utf-8'))
                    print('turn left')
                                      #ser.write(bytes('W\n','utf-8'))
                    #time.sleep(0.5)

                if  eachPrediction == 'TR':
                    turn = 0
                    ser.write(bytes('Y\n', 'utf-8'))
                    print('turn right')
                    turn = 0
                    
                    

                if  eachPrediction == 'RE':
                    if turn == 0:
                        turn = 1
                        ser.write(bytes('T\n','utf-8'))
                        print('Return')
                            
                    else:
                        ser.write(bytes('W\n','utf-8'))
                    

ser.write(bytes('Q\n','utf-8'))
# Release everything if job is finished
cap.release()
cv2.destroyAllWindows

