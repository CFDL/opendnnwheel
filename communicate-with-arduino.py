import serial

with serial.Serial('/dev/ttyACM0', 9600, timeout=10) as ser:
    while True:
        move = input('')[0]
        if move in 'wW':
            ser.write(bytes('W\n','utf-8'))
            print('forward')
        if move in 'sS':
            ser.write(bytes('S\n','utf-8'))
            print('backward')
        if move in 'aA':
            ser.write(bytes('A\n','utf-8'))
            print('left')                                                            
        if move in 'dD':
            ser.write(bytes('D\n','utf-8'))
            print('right')
        if move in 'qQ':
            ser.write(bytes('Q\n','utf-8'))
            print('stop')
        if move in 'xX':
            ser.write(bytes('X\n','utf-8'))
            print('slow down')
        if move in 'zZ':
            ser.write(bytes('Z\n','utf-8'))
            print('speed up')
        if move in 'tT':
            ser.write(bytes('T\n','utf-8'))
            print('Return')
        if move in 'rR':
            ser.write(bytes('R\n','utf-8'))
            print('Left 90')
        if move in 'yY':
            ser.write(bytes('Y\n','utf-8'))
            print('Right 90')

