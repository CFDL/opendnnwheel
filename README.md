# Overview:
This document is a guide to the recreation of the research project conducted by students of the Joint Institute for Computational Sciences at the University of Tennessee Knoxville funded by the National Science Foundation. In this project, machine learning and artificial intelligence techniques were utilized with the electrical engineering and computer science skills of the (RECSEM REU) Summer Research Experience for Undergraduates 2019 Jetson Nano - Autonomous Vehicle Team. Undergraduate and High School students in STEM fields of study, such as computer science and electrical engineering, worked together as part of a team to accomplish a task through problem solving and cooperation. Follow this manual to recreate the Jetson Nano - Autonomous Vehicle project.

[![Jetson Nano Car Demo](http://img.youtube.com/vi/TGUN6pmfTYI/0.jpg)](https://www.youtube.com/watch?v=TGUN6pmfTYI "Jetson Nano Car Demo")
# Parts Used: 

### - Nvidia Jetson Nano Developer kit:
![image](https://drive.google.com/uc?export=view&id=1Ei_nplzDr4UnCdXvedGv-rhjZZRnBxHs)

https://developer.nvidia.com/embedded/jetson-nano-developer-kit

### - Micro-SD card (64 GB):
![image](https://drive.google.com/uc?export=view&id=1BmE4q-JDGu-9ZUPzGgGEIo3x0h5ggN-4)

https://www.amazon.com/SanDisk-128GB-microSDXC-Memory-Adapter/

User will need a way to connect the Micro-SD card to the computer that will be used in this process, separate from the Jetson Nano.

### - USB Micro-SD Reader:
![image](https://drive.google.com/uc?export=view&id=1tmWPHvqDRW7_sLksIdnDetxP8OD0um-4)

https://www.amazon.com/Reader-Laptop-Windows-Chrome-RS-MMC/

### - 5V 2A Micro-USB power supply:
![image](https://drive.google.com/uc?export=view&id=1wVinWp6_vcC8kiFneyomjH941C5sCgXE)

https://www.amazon.com/INIU-Portable-External-Powerbank-Compatible/

### - RC Car Kit:
![image](https://drive.google.com/uc?export=view&id=10u0dEp0bccRPIpc9_x7cK0WdFxyKFosm)

https://www.elegoo.com/product/arduinocarv3-0/

### - Raspberry Pi Camera Module V2:
![image](https://drive.google.com/uc?export=view&id=1b3yRemphkP_MPZT8OVcgG4dum5yqfYFr)

https://www.amazon.com/Raspberry-Pi-Camera-Module-Megapixel/

### - Wireless N USB Adapter (WiFi USB Adapter):
![image](https://drive.google.com/uc?export=view&id=1n41l-pgJZITA-P0agOR74veITGyDj0Rr)

https://www.amazon.com/Panda-300Mbps-Wireless-USB-Adapter/

### - Any USB Mouse
### - Any USB Keyboard
### -Any Monitor with HDMI or Display Port
### -Access to 3D printer

### - Suggested User ID & PW: 
- nano1      nano2019
- nano2    nano2019






# A. Initial Setup
#### a. The following procedure will be carried out using a seperate computer/PC than the Jetson Nano until notified otherwise.
#### b. Navigate to official NVIDIA site for official Jetson Nano Setup Guide:
![image](https://drive.google.com/uc?export=view&id=1WcP_dnXwSxYXmnB9BHoNTwlyUzhGVKqu)

https://developer.nvidia.com/embedded/learn/get-started-jetson-nano-devkit
#### c. Download Etcher onto a separate computer:
![image](https://drive.google.com/uc?export=view&id=1GOdnF0-zMVb-6tyFD1xowrAM5k-r-8xB)

https://www.balena.io/etcher/
#### d. Download the ubuntu image provided by Nvidia:
![image](https://drive.google.com/uc?export=view&id=1UmaircJZyNbR4z8hyzKJqEZY4DzEGnh2)

https://developer.nvidia.com/embedded/dlc/jetson-nano-dev-kit-sd-card-image
**!!!Do Not Unzip!!!**

#### e. Etch image onto Jetson Nano.
##### - Plug in the Micro-SD card to your computer via USB Micro-SD connector.
##### - Open Etcher.
##### - Select the location of the image:
	/Downloads/jetson-nano-sd-r32.1-2019-03-18.zip
##### -  Choose the Micro-SD card as the drive.
##### - Press: “flash”.
##### - When finished, remove the Micro-SD card and insert it into the Jetson Nano.
This can be found flush with the side of the CPU housing unit, opposite side of the black heat dispenser. The Micro-SD will be inserted label facing away from black heat dispenser.

#### f. Connect the Jetson Nano to peripherals (monitor, mouse, keyboard) and a power supply.
#### g. Follow the prompts on the screen, and create a linux account.
#### h. Open a Terminal window on the Jetson Nano.
#### i. Since 4GB memory is limited, a swap file is needed. 
Make a 8GB swap file:
##### - If sudo permissions are ever denied:
Run: 
	
	sudo -s
This will give the user sudo (root) permissions while retaining the current user file location (directory). Enter user’s password and continue.

Then swap back to the user with: 
	
	sudo login username
Example:
 
	sudo login nano1
Enter the user’s password and continue
##### - Run: 
	sudo fallocate -l 8G /mnt/8GB.swap
##### - Run: 
	sudo mkswap /mnt/8GB.swap
##### - Run: 
	chmod 0600 /mnt/8GB.swap
##### - Run: 
	sudo swapon /mnt/8GB.swap
#### j. Once the above is working, add the following line into /etc/fstab:
##### - Call: 
	vim /etc/fstab
##### - Press “i” key to enter insert mode.
##### - To the end of the file add: 
	/mnt/8GB.swap        none    swap    sw    0 0
##### - Save and Exit (in vi/vim press the “Esc” key to get out of insert mode then type “:wq” and press “Enter”)
##### - The process for writing to a file through the vim text editor will not change.
#### k. Reboot the system.
##### - Run: 
	sudo reboot
#### l. Make sure the swap space gets mounted automatically after reboot:
##### - Run: 
	sudo mkswap /mnt/8GB.swap
##### - A message about the swap being mounted should appear.

# B. SSH Setup 
#### a. SSH is useful in accessing the Jetson Nano without the need of peripherals .
#### b. Open a terminal window on the computer/PC to be used to access the Nano.
#### c. If connecting through WiFi (recommended) (WiFi USB needed), please follow instructions in Section C.
#### d. Identify the IP address of the Nano by typing “ifconfig” on the terminal.
#### e. If plugging the Nano into your computer directly via Micro-USB or Ethernet:
##### - Find the broadcast IP on your computer with command: ifconfig
##### - Ping the broadcast with: ping -b <IP address>
Run: 

	arp -a
##### - Identify the Jetson Nano’s IP. (should start with 192.168.-.- but may not)
#### f. Use command: ssh ssh <jetson_user>@<jetson_IP_address>
Ex:    ssh nano1@10.131.9.121
#### g. Sign in.

# C. Connecting through WiFi (Recommended) (WiFi USB needed)
#### a. Connect a USB WiFi Adapter into the Jetson Nano.
#### b. Connect the Jetson Nano to peripherals.
#### c. Make sure the PC that you are using and the Jetson Nano are connected to the same WiFi network.
#### d. Identify Jetson Nano’s IP address:
##### - IP address can also be identified directly by using the Jetson Nano’s “Connection Information”.
##### - On the Jetson Nano’s desktop screen, find the WiFi connection symbol at the top right corner of the screen (left of the time, sound, etc.).
##### - Click on the WiFi symbol and a drop down menu will appear. On this menu click “Connection Information”. 
##### - A window will appear showing you all of the information about the Jetson Nano’s “Active Network Connections”. 
##### - Under the section, IPv4, the first item should be the Jetson Nano’s IP address. 
#### e. SSH into the Jetson Nano from the PC Terminal.
##### - Use command: ssh <jetson_user>@<jetson_IP_address>
Ex:    ssh nano1@10.131.9.121
#### f. Ensure that the WiFi connection is valid:
##### - Run: ping google.com
#### g. You can now proceed to Section F.

# D. Internet Connection Sharing (ICS)
#### a. If you are connected to the Jetson Nano via Micro-USB or Ethernet, you can use ICS to access the internet on the Jetson Nano.
#### b. Open a terminal window on the computer used to ssh into the Jetson Nano
Run:  

	nm-connection-editor
#### c. Click on the wired connection the Nano is using. 
Most likely the most recent connection
#### d. Click the settings icon in the bottom left.
#### e. Select the IPV4 tab and change “automatic” to “shared to other computers”.
(Do the same for IPV6)
#### f. Close the connection editor
#### g. SSH into the Jetson Nano from the PC Terminal.
##### - Use command: ssh <jetson_user>@<jetson_IP_address>
Ex:    ssh nano1@10.131.9.121
#### h. Ensure that the ICS connection is valid:
Run:

	ping google.com

# E. Enabling Hotspot
#### a. A hotspot can be used to ssh into the nano while it is driving.
#### b. From PC: ssh <jetson_user>@<jetson_IP_address>
#### c. Run: 
	nmcli device wifi hotspot con-name my-hotspot ssid my-hotspot band bg password 123456789
#### d. Now to make the hotspot always turn on
Run: 

	nmcli con show
Run: 

	nmcli con mod <connection-name> connection.autoconnect yes

# F. Necessary Installations on Jetson Nano
#### a. Make sure you are either using the Jetson Nano’s Terminal or ssh into the Jetson Nano before running these commands.

#### b. Run: 
	sudo apt install python3-pip
This will enable the pip3 command to assist in installation of python packages.

#### c. Run: 
	git clone https://github.com/patricklau12/Jetson-Nano.git
New directory Jetson-Nano should appear
	
	cd Jetson-Nano

#### d. Installing the Basics
Run: 

	./install_basics.sh

#### e. Installing OpenCV 3.4.6
##### - The opencv-3.3.1, which pre-installed on Jetson Nano, does not support gstreamer functionalities
##### - We need to remove the openCV and install OpenCV 3.4.6
Run: 

	./install_opencv-3.4.6.sh

#### f. Installing OpenBLAS
Run: 
	
	sudo apt-get install libopenblas-dev

#### g. Building TensorFlow-1.12.2
##### - Make sure swap file is set up, otherwise it may run out of memory.
##### - Make sure no problems arise by uninstalling previous version tensorboard and tensorflow
	Sudo pip3 uninstall -y tensorboard tensorflow
##### - Update protobuf for shorter model loading times
Run: 

	./install_protobuf-3.6.1.sh (~1hr)

##### - Install bazel (build tool for tensorflow)
Run: 

	./install_bazel-0.15.2.sh
##### - Install TensorFlow-1.12.2 (~10 hours)
Run: 

	./install_tensorflow-1.12.2.sh
##### - Uninstall the newer version and reinstall version 3.6.1 of python protobuf module again
Run: 

	sudo pip3 uninstall -y protobuf
Call: 
	
	cd ${HOME}/src/protobuf-3.6.1/python
Run: 

	sudo python3 setup.py install --cpp_implementation

#### h. Test TensorFlow with tf_cnn_benchmarks.py
Call: 

	cd ~/ or cd ${HOME}
Call: 

	git clone https://github.com/tensorflow/benchmarks.git
##### - A new directory “benchmarks” will appear
Call: 

	cd benchmarks
Call: 

	git checkout cnn_tf_v1.12_compatible
Run: 

	python3 scripts/tf_cnn_benchmarks/tf_cnn_benchmarks.py --data_format=NHWC --device=gpu

#### i. Installing Keras
Run: 

	sudo apt-get install keras

#### j. Installing ImageAI
Run: 

	sudo apt-get install imageai
##### MagmaDNN is not currently integrated, proceed to:
Section G. Car Setup.

#### k. Installing Magma
##### - Open Chromium on the desktop and navigate to:
https://icl.utk.edu/magma/software/index.html

##### - Download the latest version of Magma.
##### - Run: 
	vim ~/.bashrc
##### - Add the following lines to the end of ~/.bashrc :
In the following steps, the “” quotations can not effectively be copy and pasted into the terminal. When pasting into the terminal, simply delete and replace the quotation marks for correct ASCII values. If this is not done, errors will occur. 

Add: 

	export OPENBLASDIR=“/usr/lib/aarch64-linux-gnu”
Add: 

	export CUDADIR=“/usr/local/cuda”
Add: 

	export PATH=“${PATH}:/usr/local/cuda/bin”

Save and exit
Run: 

	source ~/.bashrc
##### - From ~/ 
Call: 

	cd Downloads
##### - Run the command 
	tar xvf <magma file>
##### - Call: 
	cd <magma directory>
##### - Call: 
	cp make.inc-examples/make.inc-openblas make.inc
##### - Call: 
	vim make.inc

##### - Change line with GPU_TARGET:
	GPU_TARGET = sm_53
##### - Save and exit
##### - Run: 
	sudo apt install gfortran
##### - Run: 
	make -j4
4 signifies the number of threads to be used. More threads means faster processing. 8 can be used.
##### - Run: 
	make install
##### - Test that Magma is working by navigating into the testing directory: 
Run: 

	cd testing
Run: 

	chmod u+rwx testing_sgemv
Run the testing file: 
	
	./testing_sgemv

#### l. Installing MagmaDNN
##### - Download MagmaDNN from:
https://icl.utk.edu/magma/software/index.html

##### - From home directory ( ~/ ): cd Downloads
Run: 

	tar xvf <Magmadnn filename>
Call: 

	cd <Magmadnn_Dir>
##### - Copy it to the MagmaDNN directory with: 
Call: 

	cp make.inc-examples/make.inc-example ../make.inc
“..” means previous/parent directory
Run: 

	vim make.inc
##### - Edit the specified paths as necessary and save and exit
Run: 

	make -j<number of threads>
Run: 

	make install
Run: 
	
	make testing
Call: 

	cd testing
Run: 

	sh run_tests.sh

#### m. Installing MagmaDNN
##### - Download MagmaDNN from:
https://icl.utk.edu/magma/software/index.html
##### - From home directory ( ~/ ): 
	cd Downloads
##### - Run: 
	tar xvf <Magmadnn filename>
##### - Type: 
	cd make.inc-examples
##### - Copy it to the MagmaDNN directory with: 
	cp make.inc-example /home/$USER/Downloads/magmadnn-0.2.0
##### - Type: 
	cd ..
“..” means previous/parent directory
##### - Rename the make.inc-example file to make.inc with: 
	mv make.inc-example make.inc
##### - Run: 
	vim make.inc
##### - Edit the specified paths as necessary and save and exit
##### - Run: 
	make -j<number of threads>
##### - Run: 
	make install
##### - Run: 
	make testing
##### - Call: 
	cd testing
##### - Run: 
	sh run_tests.sh

# G. Jetson Nano and Arduino Uno Serial Communication
#### a. The following procedure will be carried out using a seperate computer/PC than the Jetson Nano until notified otherwise.
#### b. Run: git clone <jetson-nano-2019 BitBucket> 
**(Change this to usable for all)**
#### c. Install Arduino IDE:
https://www.arduino.cc/en/main/software
#### d. Download the version compatible with used PC
#### e. Plug Arduino into PC via USB.
If not already done: 
	
	install pip3
Run: 
	
	sudo apt install python3-pip
#### f. Run: 
	pip3 install pyserial
#### g. Call: 
	ls -l /dev/ttyACM* 
##### - This will locate the Arduino Port. The * means fill in the blank. This accounts for any number that could be in this place. For the example, the number was found to be 0. If a different number is displayed, use that number in place of the “0”.
#### h. Run: 
	sudo chmod a+rw /dev/ttyACM0
##### - This will give permission to open the port for reading/writing data to the Arduino. If this is not done, writing/uploading to Arduino in Arduino IDE will likely fail.

#### i. This website contains information on setting up the connection: 
https://blog.rareschool.com/2019/05/five-steps-to-connect-jetson-nano-and.html
##### - Follow the instructions on this website to ensure proper working order but do not unplug the Arduino from the PC yet.

#### j. Call: 
	vim jetson-nano-2019/code/motor-control.io
#### k. Use Arduino IDE and copy/paste from motor-control.io to Arduino IDE window.
#### l. If any troubles arise refer to more detailed instructions in Section I.
#### m. Press “verify”. (check mark at the top left corner of Arduino IDE)
##### - Sometimes copy/paste doesn’t happen perfectly. Just repeat and solve issues until this compiles without error. 
#### n. Save as any desired name.
#### o. Press the “upload” button beside the “verify” button.
##### - This will write the file to the Arduino board. *Crucial
#### p. When finished, unplug Arduino from PC and plug into Jetson Nano via USB.
#### q. The following procedures will now be carried out on the Jetson Nano, either through ssh or Terminal on the Jetson Nano.
#### r. If not already done: install pip3
##### - Run: 
	sudo apt install python3-pip
##### - Run: 
	pip3 install pyserial
##### - Call: 
	ls -l /dev/ttyACM* 
This will locate the Arduino Port like before.
#### u. Run: sudo chmod a+rwx /dev/ttyACM0
This will give permission to open the port for fully manipulating/using data on the Arduino. If this is not done, the Arduino may not receive signals to actuate the motors and move the vehicle.

# H. Programming the Arduino
#### a. Upload .io code to Arduino board
##### - Connect the Arduino Uno board to PC via USB port
##### - Open Arduino IDE and work on it  
##### - “Tools” → “Board” to select “Arduino / Genuino Uno” (which is used in this project)
##### - “Tools” → “Port” to select the USB port used to connect with the Arduino board
##### - Verify the .io code to debug
##### - Upload the .io code to the Arduino board
#### b. Write .io Code in Arduino IDE
##### - Include required libraries, define I/O pins, and initialise values at the beginning
##### - Define functions to call, e.g. “void _mForward”, “void_mStop”
##### - Write the loop function in “void loop()”
#### c. Receive signal from Jetson Nano by serial communication
##### - Wait for serial port to connect by a while loop
##### - Check if we get a command: “if (Serial.available() > 0)” 
##### - Call functions for corresponding commands


# I. Camera Setup
#### a. Plug in the camera to the Jetson Nano while it is OFF
#### b. Turn ON the Jetson Nano and ssh into it with the -X argument:
##### - Call: 
	ssh -X <jetson_user>@<jetson_IP_address>
#### c. The following lines following the word “Run:” are one command line: 
##### - Run: 
	gst-launch-1.0 nvarguscamerasrc ! 'video/x-raw(memory:NVMM),width=3820, height=2464, framerate=21/1, format=NV12' ! nvvidconv ! 'video/x-raw,width=960, height=616' ! xvimagesink
This will relay a video feed to the PC. The video will most likely be upside down. Don’t worry.
#### d. Download face_detect.py from https://github.com/JetsonHacksNano/CSI-Camera
##### - Run: 
	pip3 install numpy   (only if numpy is not installed, check before run)   
##### - Run: 
	python3 face_detect.py
Change the camera configuration for quicker runtime decisions:





# J. Car Setup
#### a. Unscrew and remove the arduino, ultrasonic sensor, infrared sensor, and line tracking module.
#### b. Remove the wiring for the ultrasonic sensor, infrared sensor, and line tracking module.
#### c. Remove the cover of the battery case and remove the batteries.
#### d. Unscrew the battery case from the top of the car.
#### e. Put the batteries back in the case and unscrew the 4 screws in the standoffs at the rear of the car.
#### f. Place the battery underneath the second level of the car and replace the screws. (they will not screw all the way back in)
#### g. The camera will be placed on the front of the car within the 3D-printed camera housing unit that attaches to the 3D-printed, 3rd level of the car.

# K. Car structure design
#### a. Remove unwanted components
##### - Infrared receiver module
##### - line tracking module
##### - Bluetooth module
#### b. Add 3rd level to car for housing Jetson Nano and camera by 3D printing
![image](https://drive.google.com/uc?export=view&id=1HCt5zvibo71lnBloHINUR85Ib3J3HA2b)

3D printing
#### a. Free online 3D CAD design tool used - TinkerCAD: https://www.tinkercad.com
#### b. 3D models designed: 
##### - Jetson Nano Case:
https://www.tinkercad.com/things/iaMMmIw6ZFg (Top)

https://www.tinkercad.com/things/as5DJyBcft7 (Base)

![image](https://drive.google.com/uc?export=view&id=1QejcvFYFu-39yeVZbqv9ZVRitAScjOlK)

##### - Arduino Case:
https://www.tinkercad.com/things/3uRGU42Re4e (Base)

##### - Camera Case: 
https://www.tinkercad.com/things/5snRU8D7lh0 (Front)

https://www.tinkercad.com/things/cjZLl0TecLZ  (Back)

![image](https://drive.google.com/uc?export=view&id=1yxPnpDGYRKxI8_VHKD7BlLUSFFNCnqzu)

Arm: https://www.tinkercad.com/things/5joa2NS8N9M 

![image](https://drive.google.com/uc?export=view&id=11WsSBoQRWOlNqrRBU72w6dbBS9Jib_KJ)

##### - Mount (3rd Level): 
https://www.tinkercad.com/things/i82CN28YRd8  

![image](https://drive.google.com/uc?export=view&id=18gZBKJAT3duRpfEQQv25n0E8T0pTKUTH)
##### - Car Frame (3rd Level):
https://www.tinkercad.com/things/i82CN28YRd8 

![image](https://drive.google.com/uc?export=view&id=17uW48Hf4ucCPTMvlE5Qy8QB79FV1RCPh)

# M. Assembly with 3D Printed Parts
#### a. Replace screws on top level of RC car with extenders:
##### - Parts: Obtained from SMART ROBOT CAR KIT V3.0
![image](https://drive.google.com/uc?export=view&id=1x5mTdqyU8EaAacLH-WGCBxRG-pjHnOM7)
![image](https://drive.google.com/uc?export=view&id=1FJXoAIXnXmDLnHoJIkT2D57ckC7rel-8)
##### - Unscrew all 6 screws on top level of RC car:
##### - Replace screws with 6 extenders by hand-turning tightly:
![image](https://drive.google.com/uc?export=view&id=1mxBtxIfcZhzD7VC3hVckmzU7TKbCXCxX)

#### b. Install 3rd level for Jetson Nano and camera housing:
##### - Parts: Obtained from 3D printing
(Screws gathered from previous top level of car)

![image](https://drive.google.com/uc?export=view&id=1pt9SkFd848CIkYPoeTlKqBwjGJ9ixcEq)
![image](https://drive.google.com/uc?export=view&id=1QfBe-LrvfQlAI_8S6prSk7mHFDVKJXIq)

##### - Place 3D printed 3rd level on top of extenders, camera mount base unit facing forwards:
![image](https://drive.google.com/uc?export=view&id=1HCt5zvibo71lnBloHINUR85Ib3J3HA2b)
##### - Install screws to extenders to secure 3rd level:
![image](https://drive.google.com/uc?export=view&id=18gZBKJAT3duRpfEQQv25n0E8T0pTKUTH)

#### c. Install camera housing unit:
##### - Parts: Obtained from 3D printing
(Longer screws obtained from SMART ROBOT CAR KIT V3.0)

![image](https://drive.google.com/uc?export=view&id=1m7oxWeEA913Qit0e3wLABW2RUiz726-t)

![image](https://drive.google.com/uc?export=view&id=1yxPnpDGYRKxI8_VHKD7BlLUSFFNCnqzu)

![image](https://drive.google.com/uc?export=view&id=11WsSBoQRWOlNqrRBU72w6dbBS9Jib_KJ)

##### - Install first level connection unit, three prong down to mount prongs:
Screw semi-tightly through all 5 connecting prongs

![image](https://drive.google.com/uc?export=view&id=1ITmAypXqq9t-igbktsQ6xZX86QWiIaXj)
##### - Install second level camera unit, three prongs down:
Screw semi-tightly through all 5 connecting prongs

![image](https://drive.google.com/uc?export=view&id=1F7MZYRo-7opDERDn_c_qpDebPSl_gOIJ)
#### d. Install Jetson Nano housing unit:
##### - Parts: Obtained from 3D printing
![image](https://drive.google.com/uc?export=view&id=1QejcvFYFu-39yeVZbqv9ZVRitAScjOlK)
##### - Seed protruding columns into prepared holes in Jetson Nano housing unit, connection spaces toward car’s left side:
![image](https://drive.google.com/uc?export=view&id=1rqPh8Y4sdrfJRqCwryDPyOXNIjI99mcJ)

![image](https://drive.google.com/uc?export=view&id=1XXIUcem6d3xpIbGKjKE5uan0tjf8CrX5)
##### - Seed protruding columns into Jetson Nano board, connection ports toward car’s left side:
![image](https://drive.google.com/uc?export=view&id=1UY1BaO_MvxAZ3NQ8MVt_JOJhOceR6L_1)

#### e. Install Camera to camera housing unit:
##### - Parts:
![image](https://drive.google.com/uc?export=view&id=1pJZyRsGHJwhmRmWINTZZq2Dr2J-wElpe)

##### - Seed protruding columns into camera chip, lens towards housing unit:
![image](https://drive.google.com/uc?export=view&id=1y4hjgkk3CvkB_Nf8uhWU6vNgxlVbso7Z)
##### - Pass camera connection strip through housing unit, connector end first:
![image](https://drive.google.com/uc?export=view&id=1pMdL0kk9MmkRueyvXhO3nzTuHsUJiy7X)
##### - Connect the camera connection to the letson nano:
1. Loosen by pulling up on tab.
2. Once the camera connection is in, metal connectors facing away from the edge of the Jetson Nano board, tighten by pushing down on tab.

![image](https://drive.google.com/uc?export=view&id=1pMdL0kk9MmkRueyvXhO3nzTuHsUJiy7X)

# Running Jetson Nano Autonomous Vehicle!!!
##### - Call: 
	ssh <jetson_user>@<jetson_IP_address>
##### - Call: 	
	cd ~/jetson-nano-2019/code
##### - Run: 
	python3 test-autonomous.py
##### - Say: Ooo. Ahh.
##### - Watch in awe!

