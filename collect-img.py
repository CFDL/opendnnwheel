import numpy as np
import cv2
import os


#===================
# Define the camera
#===================
#GStreamer is used to interface with camera on the Jetson Nano. 
#The code below requests GStreamer to create a camera stream with 1920 x 1080 at 15 frames per second.
#"nvvidconv flip-method" is useful when the mounting of the camera is of a different orientation than the default.
# 0 = Default, no rotation
# 1 = Rotate 90 degrees counter-clockwise
# 2 = Rotate 180 degrees
# 3 = Rotate 90 degrees clockwise
# 4 = Flip horizontally
# 5 = Flip across upper right and lower left diagonal
# 6 = Flip vertically
# 7 = Flip across upper left and lower right diagonal
def gstreamer_pipeline (capture_width=1920, capture_height=1080, display_width=1920, display_height=1080, framerate=15, flip_method=2) :
    return ('nvarguscamerasrc ! '
    'video/x-raw(memory:NVMM), '
    'width=(int)%d, height=(int)%d, '
    'format=(string)NV12, framerate=(fraction)%d/1 ! '
    'nvvidconv flip-method=%d ! '
    'video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! '
    'videoconvert ! '
    'video/x-raw, format=(string)BGR ! appsink'  % (capture_width,capture_height,framerate,flip_method,display_width,display_height))


cap = cv2.VideoCapture(gstreamer_pipeline(flip_method=2), cv2.CAP_GSTREAMER)

#============================
#Make directory for images
#============================
num = 0
while os.path.exists('images{}'.format(num)):
    num += 1
    
# Create file path for images
dirName = 'images{}'.format(num) 
os.mkdir(dirName)
print("Directory " , dirName ,  " Created ")

img_num = 0

while(cap.isOpened()):
    ret, frame = cap.read()
    if ret==True:
        cv2.imwrite('%s/image%d.jpg' % (dirName,img_num),frame)
        print('frame captured%d' % (img_num))
        img_num += 1
        # Specifies display size
        display = cv2.resize(frame,(640,480))
        cv2.imshow('display',display)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            print('Pressed Q')
            break
    else: 
        break



#=========================================
# Release everything if job is finished
#=========================================
cap.release()
cv2.destroyAllWindows()

